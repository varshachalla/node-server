const bodyParser = require('body-parser');
const express = require('express');
var mongoose = require('mongoose');

const employee = require('./routes/employee.route');
const student = require('./routes/student.route');
const user = require('./routes/user.route');

const app = express();

var mongoDB = 'mongodb://127.0.0.1/sample';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/users', user);
app.use('/students', student);
app.use('/employees', employee);

let port = 3000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});


