const Employee = require("../model/Employee");

exports.employee_create = function(req, res) {
  let employee = new Employee({
    name: req.body.name,
    email: req.body.email,
    department: req.body.department,
    gender: req.body.gender,
    jobTitle: req.body.jobTitle
  });
  employee.save(function(err, employee) {
    if (err) {
      res.send("error");
    }
    res.send(employee);
  });
};

exports.employee_details = function(req, res) {
  Employee.findById(req.params.id, function(err, employee) {
    if (err) throw err;
    if (employee == null) {
      res.status(404);
      res.send();
    } else res.send(employee);
  });
};

exports.employee_findAll = function(req, res) {
  Employee.find({}, function(err, employees) {
    if (employees.length === 0) {
      res.status(404);
      res.send();
    } else {
      res.send(employees);
    }
  });
};

exports.employee_update = function(req, res) {
  Employee.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true },
    function(err, employee) {
      if (err) throw err;
      if (employee == null) {
        res.status(404);
        res.send();
      } else res.send(employee);
    }
  );
};


exports.employee_delete  = function(req, res) {
    Employee.findOneAndRemove({ _id: req.params.id }).exec(function(err, employee) {
      if (err) {
        return res.json({ success: false, msg: "Cannot remove item" });
      }
      if (!employee) {
        return res.status(404).json({ success: false, msg: "Employee not found" });
      }
      res.json({ success: true, msg: "Employee deleted." });
    });
  };