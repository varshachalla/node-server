const Student = require("../model/Student");

exports.student_create = function(req, res) {
  let student = new Student({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    department: req.body.department,
    yearOfStudy: req.body.yearOfStudy
  });
  student.save(function(err, student) {
    if (err) {
      res.send("error");
    }
    res.send(student);
  });
};

exports.student_details = function(req, res) {
  Student.findById(req.params.id, function(err, student) {
    if (err) throw err;
    if (student == null) {
      res.status(404);
      res.send();
    } else res.send(student);
  });
};

exports.student_findAll = function(req, res) {
  Student.find({}, function(err, students) {
    if (students.length === 0) {
      res.status(404);
      res.send();
    } else {
      res.send(students);
    }
  });
};

exports.student_update = function(req, res) {
  Student.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true },
    function(err, student) {
      if (err) throw err;
      if (student == null) {
        res.status(404);
        res.send();
      } else res.send(student);
    }
  );
};

exports.student_delete = function(req, res) {
  Student.findByIdAndRemove(req.params.id, function(err) {
    if (err) throw err;
    res.send("Deleted successfully!");
  });
};

exports.student_delete  = function(req, res) {
  Student.findOneAndRemove({ _id: req.params.id }).exec(function(err, Student) {
    if (err) {
      return res.json({ success: false, msg: "Cannot remove item" });
    }
    if (!Student) {
      return res.status(404).json({ success: false, msg: "Employee not found" });
    }
    res.json({ success: true, msg: "Student deleted." });
  });
};