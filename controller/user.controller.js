const User = require("../model/User");

exports.user_create = function(req, res) {
  let user = new User({
    name: req.body.name,
    email: req.body.email
  });
  user.save(function(err, user) {
    if (err) {
      res.send("error");
    }
    res.status(201);
    res.send(user);
  });
};

exports.user_details = function(req, res) {
  User.findById(req.params.id, function(err, user) {
    if (err) throw err;
    if (user == null) {
      res.status(404);
      res.send();
    } else res.send(user);
  });
};

exports.user_findAll = function(req, res) {
  User.find({}, function(err, users) {
    if (users.length === 0) {
      res.status(404);
      res.send();
    } else {
      res.send(users);
    }
  });
};

exports.user_update = function(req, res) {
  User.findByIdAndUpdate(
    req.params.id,
    { $set: req.body },
    { new: true },
    function(err, user) {
      if (err) throw err;
      if (user == null) {
        res.status(404);
        res.send();
      } else res.send(user);
    }
  );
};

exports.user_delete = function(req, res) {
  User.findOneAndRemove({ _id: req.params.id }).exec(function(err, item) {
    if (err) {
      return res.json({ success: false, msg: "Cannot remove item" });
    }
    if (!item) {
      return res.status(404).json({ success: false, msg: "User not found" });
    }
    res.json({ success: true, msg: "User deleted." });
  });
};
