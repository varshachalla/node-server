const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let EmployeeSchema = new Schema({
    name: {type: String, required: true, max: 100},
    email: {type: String, required: true, max: 30},
    department: {type: String, required: true, max: 30},
    gender: {type: String, required: true, max: 10},
    jobTitle: {type: String, required: true, max: 30}
});

module.exports = mongoose.model('Employee', EmployeeSchema);