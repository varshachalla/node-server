const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StudentSchema = new Schema({
    firstName: {type: String, required: true, max: 100},
    lastName: {type: String, required: true, max: 100},
    email: {type: String, required: true, max: 30},
    department: {type: String, required: true, max: 30},
    yearOfStudy: {type: Number, required: true}
});

module.exports = mongoose.model('Student', StudentSchema);