const express = require('express');
const router = express.Router();

const employee_controller = require('../controller/employee.controller');

router.post('/', employee_controller.employee_create);
router.get('/:id', employee_controller.employee_details);
router.get('/', employee_controller.employee_findAll);
router.put('/:id', employee_controller.employee_update);
router.delete('/:id', employee_controller.employee_delete);

module.exports = router;